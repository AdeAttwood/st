#!/usr/bin/env bash

set -e

SRC_URL="https://git.suckless.org/st"

if [ ! -d "./src" ]; then
    git clone "$SRC_URL" src
else
    (cd src && git reset --hard HEAD && git clean -fdx && git pull)
fi

cd src
git apply "../patches/ligatures.patch"
git apply "../patches/full-screen.patch"
git apply "../patches/config.patch"

make st install PREFIX=$HOME/.local
mkdir -p $HOME/.local/share/applications/
cp ../st.desktop $HOME/.local/share/applications/