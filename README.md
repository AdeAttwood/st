# Simple Terminal Build

A build and install script for [Suckless simple
terminal](https://st.suckless.org/).

## Usage

Download the source and run the `st` command. This will build and install st
with the prefix of `$HOME/.local` and install `st.desktop` into the same prefix.
If you have trouble building ensure you have the [build dependencies](#build) installed.

```bash
git clone https://gitlab.com/adeattwood/st.git /tmp/st && cd /tmp/st && bash ./st
```

Once the installed you can read more info in the manual

```bash
man st
```

## Dependencies

### Build

```bash
sudo apt install git make pkg-config gcc libharfbuzz-dev libfreetype-dev libfontconfig1-dev libx11-dev libxft-dev
```

### Package

``` bash
sudo apt install libbsd0 libc6 libexpat1 libfontconfig1 libfreetype6 libglib2.0-0 libglib2.0-dev libgraphite2-3 libharfbuzz0b libpcre3 libpng16-16 libuuid1 libx11-6 libxau6 libxcb1 libxdmcp6 libxft2 libxrender1 zlib1g
```

## Patches

- **config.patch** Costume patch for the config
- **ligatures.patch** From the [st website](https://st.suckless.org/patches/ligatures/)
- **full-screen.patch** Adapted from [l3pp4rd/st](https://github.com/l3pp4rd/st) [1bd7785](https://github.com/l3pp4rd/st/commit/1bd7785f2e6b595377066fe761bbc1724cf78855)
